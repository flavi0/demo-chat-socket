const express = require('express')
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);


app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Access-Control-Allow-Request-Method')
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE')
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE')
})

app.use(express.static('./'))
   

app.get('/', function(req, res) {
   //res.sendfile('index.html');
   res.render('/index.html')
});

users = [];
io.on('connection', function(socket) {
   console.log('A user connected');
   socket.on('setUsername', function(data) {
      if(users.indexOf(data) > -1) {
         users.push(data);
         socket.emit('userSet', {username: data});
      } else {
         socket.emit('userExists', data + ' username ya existe! Intenta otro username.');
      }
   })
});

http.listen(5003, function() {
   console.log('listening on localhost:5003');
});


/*
const express = require('express')
const app = express()
app.set("view engine", "ejs")
app.use(express.static("public"))
app.get('/', function(req, res) {
    res.send("welcome to chat app!")
})
let server
server = app.listen(3000, ()=>{
    console.log('server --3000')
})
const io = require('socket.io')(server)
io.on('connection', (socket) => {
    console.log('nueva conexion ..........')
})
*/

